import { router_landing } from "./routes/landing.route";
import { router_people } from "./routes/people.route";

export const routes = {
    '/': router_landing,
    '/people': router_people,
};
