import 'mocha';
import { default as request } from 'supertest';
import { app } from '../app';
import expect from 'expect.js'; // Use Chai for assertions
import sinon from 'sinon';

describe('People Route', function() {
  let openDBStub: sinon.SinonStub;

  beforeEach(() => {

    openDBStub = sinon.stub(require('../db'), 'openDB');

  });

  afterEach(function() {
    openDBStub.restore();
  });

  it('should retrieve a list of people', async function() {
    const mockName = 'MOCKNAME MOCKGEE';
    openDBStub.resolves({
      all: sinon.stub().resolves([ { id: 1, name: mockName } ]),
    });

    const res = await request(app).get('/people');

    expect(res.text).to.contain(mockName);
  });

  it('should create a new person', async function() {
    const newPersonData = {
      name: 'John Doe',
      title: 'Developer',
      bio: 'A bio about John Doe',
      location: 'New York',
      timezone: 'UTC-5',
      timeAtCompany: '2 years',
      skills: 'JavaScript, TypeScript',
      languages: 'English, Spanish',
    };
  
    openDBStub.resolves({
      run: sinon.stub().resolves({ lastID: 1001, }),
    });

    const res = await request(app)
      .post('/people/new')
      .send(newPersonData);

    expect(res.text).to.contain("Redirecting to /people/1001");
  });

  it('should retrieve a person by ID', async function() {
    const personId = 1;

    openDBStub.resolves({
      get: sinon.stub().resolves({
        id: personId,
        name: 'John Doe',
      }),
    });
  
    const res = await request(app).get(`/people/${personId}`);

    expect(res.text).to.contain('John Doe');
  });

  it('should update a person', async function() {
    const personId = 1;
    const updatedPersonData = {
      name: 'Updated Name',
      title: 'Updated Title',
      // Add other updated fields here
    };

    openDBStub.resolves({
      run: sinon.stub().resolves(),
    });

    const res = await request(app)
      .post(`/people/${personId}/edit`)
      .send(updatedPersonData);

    expect(res.text).to.contain("Redirecting to /people/1");
  });
});
