import { Request, Response, Router } from 'express';
import { cfg } from '../cfg';
import { openDB } from '../db';
import { layoutVariables } from '../layout';
import { Person } from '../logic/people.logic';
import { logger } from '../logger';

export const router_people = Router();

router_people.get('/', async (req: Request, res: Response) => {
  const db = await openDB();

  const people = await db.all<Person[]>(`SELECT * FROM person ORDER BY updated_at DESC`);

  res.render('people', {
    ...layoutVariables,
    title: `${ cfg.title } | People`,
    people,
  });
});

router_people.post('/new', async (req: Request, res: Response) => {

  // logger.info(JSON.stringify(req.body));
  const db = await openDB();

  const person = req.body;

  const runResult = await db.run(`
    INSERT INTO person (
      name, title, bio, location,
      timezone, time_at_company,
      skills, languages
    )
    VALUES (
      :name, :title, :bio, :location,
      :timezone, :timeAtCompany,
      :skills, :languages
    )
  `, {
    ':name': person.name,
    ':title': person.title,
    ':bio': person.bio,
    ':location': person.location,
    ':timezone': person.timezone,
    ':timeAtCompany': person.timeAtCompany,
    ':skills': person.skills,
    ':languages': person.languages
  });

  res.redirect(`/people/${ runResult.lastID }`);
  
});

router_people.get('/new', async (req: Request, res: Response) => {

  res.render('set_person', {
    ...layoutVariables,
    title: `${ cfg.title } | New Person`,
  });
});


router_people.get('/:person_id', async (req: Request, res: Response) => {

  const id = req.params.person_id;
  const db = await openDB();
  const person = await db.get<Person>(`SELECT * FROM person WHERE id = ?;`, [ id ]);

  if (!person) {
    res.redirect('/people');
    return;
  }

  res.render('person', {
    ...layoutVariables,
    title: `${ cfg.title } | ${ person?.name || 'No Person' }`,
    id,
    person,
  });
});

router_people.get('/:person_id/edit', async (req: Request, res: Response) => {

  const id = req.params.person_id;
  const db = await openDB();
  const person = await db.get<Person>(`SELECT * FROM person WHERE id = ?;`, [ id ]);

  if (!person) {
    res.redirect('/people');
    return;
  }

  res.render('set_person', {
    ...layoutVariables,
    title: `${ cfg.title } | ${ person?.name || 'No Person' }`,
    id,
    person,
  });
});

router_people.post('/:person_id/edit', async (req: Request, res: Response) => {

  const person = req.body;

  const personId = req.params.person_id;

  const db = await openDB();

  await db.run(`
    UPDATE person
    SET
        name = :name,
        title = :title,
        bio = :bio,
        location = :location,
        timezone = :timezone,
        time_at_company = :timeAtCompany,
        skills = :skills,
        languages = :languages
    WHERE id = :id
  `, {
      ':name': person.name,
      ':title': person.title,
      ':bio': person.bio,
      ':location': person.location,
      ':timezone': person.timezone,
      ':timeAtCompany': person.timeAtCompany,
      ':skills': person.skills,
      ':languages': person.languages,
      ':id': personId
  });

  res.redirect(`/people/${ personId }`);
});
