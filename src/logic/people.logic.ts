import { DB } from "../db";

export interface Person {
    id: number;
    name: string;
    title?: string;
    bio?: string;
    location?: string;
    timezone?: string;
    time_at_company?: string;
    skills?: string;
    languages?: string;
}

export async function setPeopleTable(db: DB) {

    await db.run(`
    CREATE TABLE IF NOT EXISTS person (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
        updated_at DATETIME DEFAULT CURRENT_TIMESTAMP,
        name TEXT NOT NULL,
        title TEXT,
        bio TEXT,
        location TEXT,
        timezone TEXT,
        time_at_company TEXT,
        skills TEXT,
        languages TEXT
    )`);
    // await db.run(`
    // ALTER TABLE person ADD COLUMN updated_at DATETIME DEFAULT CURRENT_TIMESTAMP;
    // ALTER TABLE person ADD COLUMN 
    // `);

}
