<!-- Describe the purpose and context of your MR -->

Closes #<!-- Place a link to your issue here for reference -->

## Changes Made

<!-- Describe the changes made in this MR, including any new features, bug fixes, or improvements. -->

## Screenshots (if applicable)

<!-- If your changes include visual modifications, provide screenshots to showcase the changes. -->

## Checklist

- [ ] Code follows the project's coding standards
- [ ] Tests have been added or updated to cover the changes
- [ ] Documentation has been updated (if applicable)
- [ ] No merge conflicts
- [ ] Linked to relevant issues or epics (e.g., "Closes #123")
- [ ] Reviewed another members code - See [CONTRIBUTING.md](/CONTRIBUTING.md#reviewing-code) for information on how to review code.

Once all the items above are complete, your merge request has been submitted, and your peers code has been reviewed please send Jared a message (in email or Discord) with the following information:

1. Your name and gitlab username
2. Why you chose the issue that you did
3. A link to your merge request
4. A link to the merge request that you reviewed
5. What did you think of this interview stage? We are looking for critical feedback to improve this process for future hiring cohorts.

## Additional Notes

<!-- Add any additional notes or context that reviewers may need to know. -->