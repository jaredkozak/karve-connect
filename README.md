# KarveConnect Group Technical Hiring Project

Welcome to the KarveConnect take-home project! This project is designed to assess your skills in a real-world, collaborative coding environment. The aim is to mimic the actual working scenario you would encounter as part of our team.

## Project Overview

KarveConnect is a basic professional networking platform built using Express and Node.js. The application currently supports fundamental features typical of a networking platform, and your task will be to enhance its functionality.

## Your Task

1. **Select and Implement a Feature/Issue:** You will be given a [list of issues and feature requests (issues)](https://gitlab.com/jaredkozak/karve-connect/-/issues) for the KarveConnect application. Each issue presents a unique challenge, allowing you to showcase your skills and approach to problem-solving. Choose a issue that aligns with your strengths or interests.

2. **Commit and Create a Merge Request:** Implement the chosen feature or fix in your local development environment. As you progress commit your changes and once completed create a merge request. Remember, no code will be merged into the main project at this time; this step is to simulate a real-world development workflow. [Here](https://gitlab.com/jaredkozak/karve-connect/-/merge_requests/1) is an example of a merge request that references an issue.

3. **Code Review:** After completing your part, you will be required to review the code of another candidate. Provide constructive feedback and engage in a cooperative review process. If there are no applications available to review yet, please wait for another candidate to submit their code.

You are allowed to use any tool you want including Chat GPT and Co Pilot. The sky is the limit.

You will be compensated with a $60 USD Visa Gift Card if you submit a merge request for your code and complete a code review.

If you have any questions, comments, or concerns about this process please send me an email or a DM on Discord (see the email for my Discord handle). During the hiring period I will be actively monitoring these services. I can't guarantee I will see an @ in GitLab.

## Key Evaluation Criteria

- Issue Selection Rationale: Explain why you chose the specific issue. Did it align with your strengths or was it a challenge you wanted to tackle?
- Understanding of Requirements: Demonstrate your ability to understand the problem and requirements before jumping into the solution.
- Communication: Clearly communicate what your merge request achieves. Your ability to explain your solution is crucial.
- Code Quality: Write clean, efficient, and well-documented code. Show your understanding of best coding practices.
- Solution Impact and Effort: Display your ability to implement impactful solutions with optimal effort. Understand and explain the trade-offs made in your approach.
- Project Understanding: Show that you understand the KarveConnect project as a whole and how your contribution fits into the larger picture.
- Code Review Skills: Engage in thoughtful and constructive code reviews. Demonstrate your ability to think critically about other people's code.
- Collaboration and Friendliness: Display a cooperative and friendly attitude during the competitive review process.

## Open Nature of the Project

We would like to inform you that this repository is public and accessible to everyone. Your valuable contributions have the potential to be integrated into the base repository, enhancing its capabilities for future hiring campaigns. As an open-source project, it is governed by the [MIT License](./LICENSE), which extends to any contributions you make. This open-source nature allows you the opportunity to showcase your work in your professional portfolio, demonstrating your skills and contributions to a wider audience.

However, we fully respect your privacy and intellectual property. If you have any concerns about your contributions being publicly accessible, please feel free to reach out to Jared. We are committed to accommodating your preferences, including the removal of your fork from the repository after the conclusion of the interview process, should you request it.

Your participation and contributions are highly valued, and we aim to ensure that this project serves as a positive and professional experience for all involved.

## Project Simplification

To make the project more accessible and focus on the core aspects of development, the following simplifications have been made:

- **Database:** The application uses a simple, text-based database.
- **Back-End:** A straightforward Express backend without a complex frontend framework.
- **No Authentication:** No authentication system is implemented, simplifying the interaction model.

## Getting Started

1. Fork the project: [Create a fork](https://gitlab.com/jaredkozak/karve-connect/-/forks/new) of the project and clone the fork to your machine. Be sure to keep the visibility level "public" for the duration of this project. You will need a GitLab account to do this, feel free to create a gitlab account under an alias if that makes you feel more comfortable.
2. Environment Setup: See [Environment Setup](#environment-setup) below
3. Familiarize Yourself: Begin by understanding the existing codebase and architecture of KarveConnect. See [Resources](#resources) for references to tools used in this application
4. Choose Your Task: Select a task from the [list of issues](https://gitlab.com/jaredkozak/karve-connect/-/issues) and begin your implementation. **Be sure to assign the task to yourself so that nobody else takes it!** If you feel that there is a task that should be worked on that is not in this list, please ask Jared for approval first.
5. Commit: As you implement your task, commit your changes.
6. Create Request: create a merge request from your fork onto the main project.
7. Review: Proceed to review another candidates code.

We are excited to see your contribution and how you tackle the challenges presented in this project. Good luck!

## Environment Setup

1. Clone the project to your machine
2. Install [node v20](https://nodejs.org/en) - you can also use [Node Version Manager](https://github.com/nvm-sh/nvm). Just run `nvm install` and `nvm use` after you clone the project. Node 14+ should also work if you already have a version of node installed on your machine.
3. Run `npm install` to install pre-requisites from package.json
4. Run `npm run dev` to launch the dev version of the application which will restart when you change code automatically

That's it! You should be good to proceed.

## Resources

- Language: [NodeJS](https://nodejs.org/en)
- Typescript: [TypeScript](https://www.typescriptlang.org/)
- HTTP Server: [Express](https://expressjs.com/)
- HTML Parser: [Pug](https://pugjs.org/api/getting-started.html) <<<--- You may not be familar with this one
- Database: [SQLite3](https://www.sqlitetutorial.net/sqlite-nodejs/)
